<?php

use BookModel;


/**
 * Class BookService
 */
class BookService
{
    /**
     * @param $data
     */
    public function search($data)
    {
        return BookModel::search($data);
    }

    /**
     * @param $sortBy
     */
    public function getAll($sortBy)
    {
        return BookModel::all($sortBy);
    }

    /**
     * @param $id
     * @param $data
     */
    public function updateBook($id, $data)
    {
        return BookModel::update($id, $data);
    }

    /**
     * @param $id
     */
    public function deleteBook($id)
    {
        return BookModel::delete($id);
    }

    public function createBook($data)
    {
        return BookModel::create($data);
    }
}