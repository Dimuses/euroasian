<?php

use BookService;

class BookController
{
    private $booksService;

    /**
     * BookController constructor.
     * @param \BookService $bookService
     */
    public function __construct(BookService $bookService)
    {
        $this->booksService = $bookService;
    }


    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $data = $request->getData();
        $this->booksService->createBook($data);
    }

    /**
     * @param int $sortBy
     * @return
     */
    public function index($sortBy = SORT_ASC)
    {
        return $this->booksService->getAll($sortBy);
    }

    /**
     * @param Request $request
     * @param $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $data = $request->getData();
        return $this->booksService->updateBook($id, $data);
    }

    /**
     * @param $id
     * @return
     */
    public function delete($id)
    {
        return $this->booksService->deleteBook($id);
    }

    /**
     * @param Request $request
     * @return

     +*/
    public function search(Request $request)
    {
        $query = $request->getQueryParams();
        return $this->booksService->search($query);
    }
}