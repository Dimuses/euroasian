<?php


/**
 * Class BookModel
 */
class BookModel
{

    /**
     * @param $data
     */
    public static function search($data)
    {
    }

    /**
     * @param $sortBy
     */
    public static function all($sortBy)
    {
    }

    /**
     * @param $id
     * @param $data
     */
    public static function update($id, $data)
    {
    }

    /**
     * @param $id
     */
    public static function delete($id)
    {
    }

    /**
     * @param $data
     */
    public static function create($data)
    {
    }
}